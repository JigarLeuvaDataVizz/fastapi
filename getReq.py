from pydantic import BaseModel
from fastapi import FastAPI
import pymysql
import json

app = FastAPI()

conn = pymysql.connect(
host='localhost',
user='root', 
password = "root",
db='testdb')



class create_dict(dict): 
    def __init__(self): 
        self = dict() 
          
    def add(self, key, value): 
        self[key] = value


@app.get("/user/{personid}")
async def root(personid):
    mydict=create_dict()
    cur = conn.cursor()
    query="select * from person where personid= %s" 
    cur.execute(query,personid)

    output = cur.fetchall()


    for row in output:
        mydict.add(row[0],({"id":row[0],"Name":row[1],"phone":row[2]}))      


    stud_json = json.dumps(mydict, indent=2, sort_keys=True)

    print(stud_json)

    return stud_json

class myperson(BaseModel):
    personid:int
    pname:str
    pphn:int

@app.post("/user/i")
async def root2(myp:myperson):
    mydict=create_dict()
    cur = conn.cursor()
    query="INSERT INTO person(personid,pname,pphn) VALUES (%s,%s,%s)"
    cur.execute(query,(myp.personid,myp.pname,myp.pphn))
    conn.commit()
    print(query)
   
    result=cur.execute("select * from person")
    print(result)

    output = cur.fetchall()


    for row in output:
        mydict.add(row[0],({"id":row[0],"Name":row[1],"phone":row[2]}))      


    stud_json = json.dumps(mydict, indent=2, sort_keys=True)
    file = stud_json.replace('\n', '')    
    data = json.loads(file)
    print(data)
    return data

    